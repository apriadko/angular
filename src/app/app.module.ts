import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './user/user.component';
import {UsersService} from './users.service';
import { RolesComponent } from './roles/roles.component';
import { RoleComponent } from './role/role.component';
import {RolesService} from './roles.service';


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    RolesComponent,
    RoleComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    UsersService,
    RolesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
