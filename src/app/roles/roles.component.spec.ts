import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';

import {RolesComponent} from './roles.component';
import {RolesService} from '../roles.service';

describe('RolesComponent', () => {
  let component: RolesComponent;
  let fixture: ComponentFixture<RolesComponent>;
  let service: RolesService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolesComponent ],
      providers: [RolesService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolesComponent);
    component = fixture.componentInstance;
    service = TestBed.get(RolesService);
    fixture.detectChanges();
  });

  it('Service injected via inject(...) and TestBed.get(...) should be the same instance',
    inject([RolesService], (injectService: RolesService) => {
      expect(injectService).toBe(service);
    })
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
