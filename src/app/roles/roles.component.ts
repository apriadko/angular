import {Component, OnInit} from '@angular/core';
import {RolesService} from '../roles.service';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent implements OnInit {

  roles;

  constructor(service: RolesService) {
    this.roles = service.getRoles();
  }

  ngOnInit() {
  }

}
