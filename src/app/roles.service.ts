import {Injectable} from '@angular/core';

@Injectable()
export class RolesService {

  constructor() {
  }

  getRoles() {
    return ['admin', 'user'];
  }
}
