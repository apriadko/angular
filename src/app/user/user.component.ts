import {Component, Input, OnInit} from '@angular/core';

export class Role {
  name: String;
}

export class User {
  login: String;
  firstName: String;
  lastName: String;
  roles: Set<Role>;
}

@Component({
  selector: '[user]',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  @Input('user') user: User;

  constructor() {
  }

  ngOnInit() {
  }

}
