import {Injectable} from '@angular/core';
import {Role, User} from './user/user.component';

@Injectable()
export class UsersService {

  constructor() {
  }

  getUsers() {
    const role1 = new Role();
    role1.name = 'user';
    const role = new Role();
    role.name = 'admin';


    const user = new User();
    user.firstName = 'first name';
    user.lastName = 'last name ';
    user.login = 'admin';
    user.roles = new Set<Role>();
    user.roles.add(role1);
    user.roles.add(role);

    const user1 = new User();
    user1.firstName = 'Alex';
    user1.lastName = 'P';
    user1.login = 'admin';
    user1.roles = new Set<Role>();

    user1.roles.add(role1);

    return [user, user1];
  }
}
