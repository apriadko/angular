import {async, ComponentFixture, TestBed, inject} from '@angular/core/testing';

import {UsersComponent} from './users.component';
import {UsersService} from '../users.service';

describe('UsersComponent', () => {
  let component: UsersComponent;
  let fixture: ComponentFixture<UsersComponent>;
  let service: UsersService;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      declarations: [UsersComponent],
      providers: [UsersService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersComponent);
    component = fixture.componentInstance;
    service = TestBed.get(UsersService);
    fixture.debugElement.injector.get(UsersService);
    fixture.detectChanges();

  });

  // it('should render title in a h1 tag', async(() => {
  //   fixture = TestBed.createComponent(UsersComponent);
  //   fixture.detectChanges();
  //   const compiled = fixture.debugElement.nativeElement;
  //   expect(compiled.querySelector('h1').textContent).toContain('Users :');
  // }));
});
